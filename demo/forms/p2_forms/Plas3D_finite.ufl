#!/usr/bin/env python
# Copyright (C) 2009-2011 Kristian B. Oelgaard and Garth N. Wells.
# Licensed under the GNU LGPL Version 3.

scheme = "default"
degree = 3
dx = Measure("dx")
dx = dx(degree=degree, rule=scheme)

elementA = VectorElement("Lagrange", triangle, 2)
elementT = VectorElement("Quadrature", triangle, degree, dim=81,
                         quad_scheme=scheme)
elementS = VectorElement("Quadrature", triangle, degree, dim=9,
                         quad_scheme=scheme)

v = TestFunction(elementA)
u = TrialFunction(elementA)
f = Coefficient(elementA)
t = Coefficient(elementT)
s = Coefficient(elementS)


def grad_vect(u):

    term= grad(u)

    return as_vector([term[0,0], term[1,1], term[2,2], term[0,1], term[1,0], term[1,2], term[2,1], term[0,2], term[2,0]])


def sigma(s):
    return as_vector([s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8]])

def tangent(t):
    return as_matrix([  [t[0], t[3], t[5],     t[6],  t[7],  t[8],  t[9],  t[10], t[11]],
			[t[3], t[1], t[4],     t[12], t[13], t[14], t[15], t[16], t[17]],
			[t[5], t[4], t[2],     t[18], t[19], t[20], t[21], t[22], t[23]],

			[t[6], t[12], t[18],   t[24], t[25], t[26], t[27], t[28], t[29]],
			[t[7], t[13], t[19],  t[25],  t[30], t[31], t[32], t[33], t[34]],
			[t[8], t[14], t[20],  t[26], t[31],  t[35], t[36], t[37], t[38]],
			[t[9], t[15], t[21],  t[27], t[32], t[36],  t[39], t[40], t[41]],
			[t[10], t[16], t[22], t[28], t[33], t[37], t[40],  t[42], t[43]],
			[t[11], t[17], t[23], t[29], t[34], t[38], t[41], t[43],  t[44]]  ])


a = dot(grad_vect(v), dot(tangent(t), grad_vect(u)))*dx
L = dot(grad_vect(v), sigma(s))*dx - inner(v, f)*dx
