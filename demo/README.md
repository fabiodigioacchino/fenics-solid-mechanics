# Demo programs

To compile and run the demo progrms, first compile the form files:

    > cd forms
    > ./compile_forms

FFC must be installed to compile the form files.

To build a demo, e.g. beam_3d:

    > cd beam_3d
    > cmake -D CMAKE_BUILD_TYPE=Release .
    > ./demo_3D_P2_beam
